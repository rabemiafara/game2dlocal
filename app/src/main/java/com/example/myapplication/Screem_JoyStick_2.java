package com.example.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.myapplication.bouton.Joystick_R;

public class Screem_JoyStick_2 extends SurfaceView implements SurfaceHolder.Callback {

    private Paint painte;

    private double position_x = 1000;
    private double position_y = 400;

//    private Paint paint;

    private int joystickPointerId_R = 0;

    private Joystick_R joystickR;

    private Gameloop_2 gameloop_2;

    public Screem_JoyStick_2(Context context) {
        super(context);

//        paint = new Paint();
//        paint.setColor(Color.BLUE);
//        paint.setAntiAlias(true);

        this.setX((float) position_x);
        this.setY((float) position_y);

//        joystickR = new Joystick_R((int)position_x,(int)position_y,30,60);
//        joystickR = new Joystick_R(50,50,20,60);

        joystickR = new Joystick_R(130,130,70,40);

        this.setLayoutParams(new ViewGroup.LayoutParams(300,300));

        SurfaceHolder surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);

        this.gameloop_2 = new Gameloop_2(this,surfaceHolder);
        this.painte = new Paint();
        this.painte.setColor(Color.TRANSPARENT);

    }



//    @Override
//    public void draw(@NonNull Canvas canvas) {
//        super.onDraw(canvas);
////        canvas.drawCircle((float) position_x,(float) position_y,60,paint);
//        joystickR.draw(canvas);
//    }

    public void update() {
        this.joystickR.update();
    }


    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawPaint(this.painte);
        joystickR.draw(canvas);

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // Handle user input touch event actions
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:

                if (joystickR.getIsPressed()) {
                    // Joystick was pressed before this event -> cast spell
                    System.out.println("boutton clicker");
                } else if (joystickR.isPressed((double) event.getX(), (double) event.getY())) {
                    // Joystick is pressed in this event -> setIsPressed(true) and store pointer id
                    joystickPointerId_R = event.getPointerId(event.getActionIndex());
                    System.out.println("boutton clicker");
                    joystickR.setIsPressed(true);
                } else {
                    // Joystick was not previously, and is not pressed in this event -> cast spell
                }


                return true;
            case MotionEvent.ACTION_MOVE:

                int pointer_counte = event.getPointerCount();

                if (joystickR.getIsPressed()) {
                    // Joystick was pressed previously and is now moved
                    joystickR.setActuator((double) event.getX(), (double) event.getY());

                }

                return true;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:

                if (joystickPointerId_R == event.getPointerId(event.getActionIndex())) {
                    // joystick pointer was let go off -> setIsPressed(false) and resetActuator()
                    joystickR.setIsPressed(false);
                    joystickR.resetActuator();
                }

                return true;
        }

        return super.onTouchEvent(event);
    }
    public void pause() {
        gameloop_2.stopLoop();
    }
    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
        Log.d("Game.java", "surfaceCreated()");
        if (gameloop_2.getState().equals(Thread.State.TERMINATED)) {
            SurfaceHolder surface = getHolder();
            surfaceHolder.addCallback(this);
            gameloop_2 = new Gameloop_2 (this, surface);
        }
        gameloop_2.startLoop();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {

    }

    public Joystick_R getJoystickR() {
        return joystickR;
    }

    public void setJoystickR(Joystick_R joystickR) {
        this.joystickR = joystickR;
    }
}
