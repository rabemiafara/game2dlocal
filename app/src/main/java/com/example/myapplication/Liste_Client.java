package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


public class Liste_Client extends AppCompatActivity {

    ListView listView;
    ArrayAdapter <String> arrayAdapter ;

    HashMap<Socket,String> clientNameList ;
    ArrayList<Socket> clients;

    Button lancer_game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listView = new ListView(this);

        String [] liste_s = null ;

        if(MainActivity.etat_connection == MainActivity.Etat_connection.HOST){
            liste_s = MainActivity.getListeJoueurServer();
            MainActivity.listeJoueur = MainActivity.getListeJoueurServer();
        } else if(MainActivity.etat_connection == MainActivity.Etat_connection.CLIENT) {
            liste_s = MainActivity.listeJoueur ;



            try {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        boolean bb = false;

                        while (true){
                            if(MainActivity.etatJeu == MainActivity.Etat_Jeu.LANCER){
                                bb = true;
                                break;
                            }
                        }
                        if(bb == true){
                            Intent intent = new Intent(Liste_Client.this, GameActivity.class);
                            startActivity(intent);
                        }
                    }
                }).start();
            } catch (Exception e){
                e.printStackTrace();
            }

        } else {
            Intent intent = new Intent(Liste_Client.this, Choix_jeu.class);
            startActivity(intent);
        }

        arrayAdapter  = new ArrayAdapter<>(this,R.layout.grid_item_layout,liste_s);

        listView.setAdapter(arrayAdapter);

        LinearLayout linearLayout = new LinearLayout(this);

        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        ));

        linearLayout.setOrientation(LinearLayout.VERTICAL);

        if(MainActivity.etat_connection == MainActivity.Etat_connection.HOST){
            lancer_game = new Button(this);
            lancer_game.setText("lancer le jeu");
            lancer_game.setX(10);
            lancer_game.setY(20);

            lancer_game.setOnClickListener(new View.OnClickListener() {
                private int randomPos(){
                    return new Random().nextInt(500) + 20 ;
                }

                @Override
                public void onClick(View v) {
                    System.out.println("lancement du jeu");
                    MainActivity.networkThread.lancerJeu();
                    MainActivity.etatJeu = MainActivity.Etat_Jeu.LANCER;
                    ArrayList<Socket> listeJoueur = MainActivity.networkThread.getClients();
                    for(int i=0;i<listeJoueur.size() ;i++){
                        MainActivity.networkThread.sendMessage(MainActivity.CHANG_POS_PRE+randomPos() + "," + randomPos(),listeJoueur.get(i));
                    }
                    MainActivity.positionX = randomPos();
                    MainActivity.positionY = randomPos();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            boolean bb = false;
                            while (true){
                                if(MainActivity.etatJeu == MainActivity.Etat_Jeu.LANCER){

                                    try {
                                        if(MainActivity.listeJoueur.length!=0){
                                            bb = true;
                                            break;
                                        } else {
                                            MainActivity.client.evoyerMessage(MainActivity.REQ_LIST);
                                        }
                                    } catch (Exception e){
                                        MainActivity.client.evoyerMessage(MainActivity.REQ_LIST);
                                    }
                                }
                            }
                            if(bb==true){
                                Intent intent = new Intent(Liste_Client.this, GameActivity.class);
                                startActivity(intent);
                            }
                        }
                    }).start();

                }
            });
            linearLayout.addView(lancer_game);
        }

        linearLayout.addView(listView);

        setContentView(linearLayout);

    }

}






