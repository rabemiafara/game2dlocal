package com.example.myapplication.obstacle;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.myapplication.Plan_secondaire;

public class Obstacle {

    private double position_x ;
    private double position_y ;

    private double position_screem_x;
    private double position_screem_y;

    private Plan_secondaire plan_secondaire;

    private Paint paint;

    private double radius;

    public Obstacle(double position_x, double position_y, Plan_secondaire plan_secondaire) {

        paint = new Paint();

        paint.setColor(Color.YELLOW);

        this.position_x = position_x;
        this.position_y = position_y;
        this.plan_secondaire = plan_secondaire;

        this.position_screem_x = this.plan_secondaire.get_x_screen(this.position_x);
        this.position_screem_y = this.plan_secondaire.get_y_screen(this.position_y);

        radius = 150;

    }

    public boolean is_containse(double x , double y ) {
        double distance = Math.sqrt(
                Math.pow( x - position_x , 2) +
                        Math.pow(y - position_y, 2)
        );
        return distance >= radius ;
    }

    public void draw(Canvas canvas){
        canvas.drawCircle((float) this.position_screem_x,(float) this.position_screem_y,(float) radius,paint);
    }

    public void update() {
        this.position_screem_x = this.plan_secondaire.get_x_screen(this.position_x);
        this.position_screem_y = this.plan_secondaire.get_y_screen(this.position_y);
    }

    public double getPosition_x() {
        return position_x;
    }

    public void setPosition_x(double position_x) {
        this.position_x = position_x;
    }

    public double getPosition_y() {
        return position_y;
    }

    public void setPosition_y(double position_y) {
        this.position_y = position_y;
    }

    public double getPosition_screem_x() {
        return position_screem_x;
    }

    public void setPosition_screem_x(double position_screem_x) {
        this.position_screem_x = position_screem_x;
    }

    public double getPosition_screem_y() {
        return position_screem_y;
    }

    public void setPosition_screem_y(double position_screem_y) {
        this.position_screem_y = position_screem_y;
    }

    public Plan_secondaire getPlan_secondaire() {
        return plan_secondaire;
    }

    public void setPlan_secondaire(Plan_secondaire plan_secondaire) {
        this.plan_secondaire = plan_secondaire;
    }
}
