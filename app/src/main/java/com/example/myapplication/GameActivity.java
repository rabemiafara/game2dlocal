package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myapplication.page.Acceuille_page;

public class GameActivity extends AppCompatActivity {
    private Screem_JoyStick_2 screem_joyStick_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        ));
        TextView textView = new TextView(this);
        textView.setText("game en cours");
        linearLayout.addView(textView);

        DisplayMetrics displayMetrics = new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
        ));

        screem_joyStick_2 = new Screem_JoyStick_2(this);

        MainActivity.game = new Game(this,(double)width,(double)height,screem_joyStick_2);


//        screem_joyStick_2.setZOrderOnTop(true);
        screem_joyStick_2.getHolder().setFormat(PixelFormat.TRANSPARENT);
        screem_joyStick_2.setBackgroundColor(Color.TRANSPARENT);

        frameLayout.addView(MainActivity.game);
        frameLayout.addView(screem_joyStick_2);

        Acceuille_page acceuille = new Acceuille_page();

        setContentView(acceuille.ecrand_acceuille(this));

        setContentView(frameLayout);

    }
}

