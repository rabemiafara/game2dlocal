package com.example.myapplication.personnage;

import android.content.Context;
import android.graphics.Canvas;

import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.myapplication.bouton.Joystick_R;

public class Perso extends SurfaceView implements SurfaceHolder.Callback {

    private double position_x = 1000;
    private double position_y = 400;

//    private Paint paint;

    private int joystickPointerId_R = 0;

    private Joystick_R joystickR;

    public Perso(Context context) {
        super(context);

//        paint = new Paint();
//        paint.setColor(Color.BLUE);
//        paint.setAntiAlias(true);

        this.setX((float) position_x);
        this.setY((float) position_y);

//        joystickR = new Joystick_R((int)position_x,(int)position_y,30,60);
        joystickR = new Joystick_R(0,0,30,60);

        this.setLayoutParams(new ViewGroup.LayoutParams(90,90));

    }



//    @Override
//    public void draw(@NonNull Canvas canvas) {
//        super.onDraw(canvas);
////        canvas.drawCircle((float) position_x,(float) position_y,60,paint);
//        joystickR.draw(canvas);
//    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        joystickR.draw(canvas);
        // Draw Tilemap
//        drawFPS(canvas);
//        drawUPS(canvas);
//        joystickL.draw(canvas);
//        player.draw(canvas);
//        obstacle.draw(canvas);
//        avatar.draw(canvas);

    }


    public void change_coordonner(double x,double y){
        this.position_x = x;
        this.position_y = y;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // Handle user input touch event actions
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:

                if (joystickR.getIsPressed()) {
                    // Joystick was pressed before this event -> cast spell
                    System.out.println("boutton clicker");
                } else if (joystickR.isPressed((double) event.getX(), (double) event.getY())) {
                    // Joystick is pressed in this event -> setIsPressed(true) and store pointer id
                    joystickPointerId_R = event.getPointerId(event.getActionIndex());
                    System.out.println("boutton clicker");
                    joystickR.setIsPressed(true);
                } else {
                    // Joystick was not previously, and is not pressed in this event -> cast spell
                }


                return true;
            case MotionEvent.ACTION_MOVE:

                int pointer_counte = event.getPointerCount();

                for (int i=0;i<pointer_counte;i++){
                    int id = event.getPointerId(i);
                    if(id == joystickPointerId_R){
                        if (joystickR.getIsPressed()) {
                            // Joystick was pressed previously and is now moved
                            joystickR.setActuator((double) event.getX(), (double) event.getY());
                        }
                    }
                }

                return true;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:

                if (joystickPointerId_R == event.getPointerId(event.getActionIndex())) {
                    // joystick pointer was let go off -> setIsPressed(false) and resetActuator()
                    joystickR.setIsPressed(false);
                    joystickR.resetActuator();
                }

                return true;
        }

        return super.onTouchEvent(event);
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {

    }



}
