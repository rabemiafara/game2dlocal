package com.example.myapplication.bouton;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.myapplication.utilisateur.Player;

public class Joystick_L {

    private int outerCircleCenterPositionX;
    private int outerCircleCenterPositionY;
    private int innerCircleCenterPositionX;
    private int innerCircleCenterPositionY;

    private int outerCircleRadius;
    private int innerCircleRadius;

    private Paint innerCirclePaint;
    private Paint outerCirclePaint;
    private boolean isPressed = false;
    private double joystickCenterToTouchDistance;
    private double actuatorX;
    private double actuatorY;

    private Player player;

    public Joystick_L(int centerPositionX, int centerPositionY, int outerCircleRadius, int innerCircleRadius) {

        // Outer and inner circle make up the joystick
        outerCircleCenterPositionX = centerPositionX;
        outerCircleCenterPositionY = centerPositionY;
        innerCircleCenterPositionX = centerPositionX;
        innerCircleCenterPositionY = centerPositionY;

        // Radii of circles
        this.outerCircleRadius = outerCircleRadius;
        this.innerCircleRadius = innerCircleRadius;

        // paint of circles
        outerCirclePaint = new Paint();
        outerCirclePaint.setColor(Color.GRAY);
        outerCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);

        innerCirclePaint = new Paint();
        innerCirclePaint.setColor(Color.BLUE);
        innerCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    public void draw(Canvas canvas) {
        // Draw outer circle
        canvas.drawCircle(
                outerCircleCenterPositionX,
                outerCircleCenterPositionY,
                outerCircleRadius,
                outerCirclePaint
        );

        // Draw inner circle
        canvas.drawCircle(
                innerCircleCenterPositionX,
                innerCircleCenterPositionY,
                innerCircleRadius,
                innerCirclePaint
        );


    }

    public void update() {
        updateInnerCirclePosition();
    }

    private void updateInnerCirclePosition() {
        innerCircleCenterPositionX = (int) (outerCircleCenterPositionX + actuatorX*outerCircleRadius);
        innerCircleCenterPositionY = (int) (outerCircleCenterPositionY + actuatorY*outerCircleRadius);
    }

//    public void bouge(double touchPositionX, double touchPositionY){
//        double deltaX = touchPositionX - outerCircleCenterPositionX;
//        double deltaY = touchPositionY - outerCircleCenterPositionY;
//        double deltaDistance = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
//
//        if(deltaDistance < outerCircleRadius) {
//            actuatorX = deltaX/outerCircleRadius;
//            actuatorY = deltaY/outerCircleRadius;
//        } else {
//            actuatorX = deltaX/deltaDistance;
//            actuatorY = deltaY/deltaDistance;
//        }
//        updateInnerCirclePosition();
//
////        System.out.println("equation de la droite => y="
////                +player.coefficient_directeur(innerCircleCenterPositionX,actuatorX,innerCircleCenterPositionY,actuatorY)
////                +"x + "+player.ordonner_origine(innerCircleCenterPositionX,actuatorX,innerCircleCenterPositionY,actuatorY)
////        );
//
//    }

    public void touch_up(){
        actuatorX = outerCircleCenterPositionX + 0 ;
        actuatorY = outerCircleCenterPositionY + 0 ;

        updateInnerCirclePosition();

    }

    public void setActuator(double touchPositionX, double touchPositionY) {
        double deltaX = touchPositionX - outerCircleCenterPositionX;
        double deltaY = touchPositionY - outerCircleCenterPositionY;
        double deltaDistance = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));

        if(deltaDistance < outerCircleRadius) {
            actuatorX = deltaX/outerCircleRadius;
            actuatorY = deltaY/outerCircleRadius;
        } else {
            actuatorX = deltaX/deltaDistance;
            actuatorY = deltaY/deltaDistance;
        }
    }

    public boolean isPressed(double touchPositionX, double touchPositionY) {
        joystickCenterToTouchDistance = Math.sqrt(
            Math.pow(outerCircleCenterPositionX - touchPositionX, 2) +
            Math.pow(outerCircleCenterPositionY - touchPositionY, 2)
        );
        return joystickCenterToTouchDistance < outerCircleRadius;
    }

    public boolean getIsPressed() {
        return isPressed;
    }

    public void setIsPressed(boolean isPressed) {
        this.isPressed = isPressed;
    }

    public double getActuatorX() {
        return actuatorX;
    }

    public double getActuatorY() {
        return actuatorY;
    }

    public void resetActuator() {
        actuatorX = 0;
        actuatorY = 0;
    }


    public int getOuterCircleCenterPositionX() {
        return outerCircleCenterPositionX;
    }

    public void setOuterCircleCenterPositionX(int outerCircleCenterPositionX) {
        this.outerCircleCenterPositionX = outerCircleCenterPositionX;
    }

    public int getOuterCircleCenterPositionY() {
        return outerCircleCenterPositionY;
    }

    public void setOuterCircleCenterPositionY(int outerCircleCenterPositionY) {
        this.outerCircleCenterPositionY = outerCircleCenterPositionY;
    }

    public int getInnerCircleCenterPositionX() {
        return innerCircleCenterPositionX;
    }

    public void setInnerCircleCenterPositionX(int innerCircleCenterPositionX) {
        this.innerCircleCenterPositionX = innerCircleCenterPositionX;
    }

    public int getInnerCircleCenterPositionY() {
        return innerCircleCenterPositionY;
    }

    public void setInnerCircleCenterPositionY(int innerCircleCenterPositionY) {
        this.innerCircleCenterPositionY = innerCircleCenterPositionY;
    }


    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
