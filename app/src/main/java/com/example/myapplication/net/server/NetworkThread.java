package com.example.myapplication.net.server;

import android.os.AsyncTask;
import com.example.myapplication.MainActivity;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class NetworkThread extends AsyncTask<Void,Void,Void> {
    ArrayList<Socket> clients = new ArrayList<>();
    HashMap<Socket, String> clientNameList = new HashMap<Socket, String>();
    private PrintWriter printWriter;
    private BufferedReader in;

    private ServerSocket serversocket;

    public NetworkThread(){

    }


    public void lancer() throws IOException {
        clients = new ArrayList<>();
        clientNameList = new HashMap<Socket, String>();
        try {

            ServerSocket serversocket = new ServerSocket(5000) ;
            System.out.println("Server is started...");
            while (true) {
                Socket socket = serversocket.accept();
                clients.add(socket);
                ThreadServer ThreadServer = new ThreadServer(socket, clients, clientNameList);
                ThreadServer.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected Void doInBackground(Void... voids) {
        try {
            System.out.println("lancer une lance bebe");
            lancer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HashMap<Socket,String> getClientNameList(){
        return this.clientNameList;
    }

    public ArrayList<Socket> getClients() {
        return clients;
    }

    public void setClients(ArrayList<Socket> clients) {
        this.clients = clients;
    }


    public void lancerJeu(){
        sendMessage(MainActivity.LANCER_JEU);
    }

    public void sendMessage(String message,Socket socket){
        new Thread(new Runnable() {
            @Override
            public void run() {
                PrintWriter printWriter;
                try {
                    printWriter = new PrintWriter(socket.getOutputStream(), true);
                    printWriter.println(message);
                } catch (IOException ex) {
                    System.out.println(ex);
                }
            }
        }).start();
    }

    public void sendMessage(String message){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Socket socket;
                PrintWriter printWriter;
                int i = 0;
                while (i < clients.size()){
                    socket = clients.get(i);
                    i++;
                    try {
                        printWriter = new PrintWriter(socket.getOutputStream(), true);
                        printWriter.println(message);

                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                }
//////////////////////////////////////////////////////////////
            }
        }).start();

    }

//    public void sendAllMessage(String message){
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Socket socket;
//                PrintWriter printWriter;
//                int i = 0;
//                while (i < clients.size()){
//                    try {
//                        printWriter = new PrintWriter(clients.get(i).getOutputStream() , true);
//                        printWriter.println(message);
//                        i++;
//                    } catch (IOException ex) {
//                        System.out.println(ex);
//                    }
//                }
////////////////////////////////////////////////////////////////
//            }
//        }).start();
//    }

}
