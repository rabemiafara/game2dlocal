package com.example.myapplication.net.client;

import com.example.myapplication.MainActivity;
import com.example.myapplication.utilisateur.Communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;


/**
 * Thread for clients
 */
public class ThreadClient implements Runnable {

    private Socket socket;
    private BufferedReader cin;

    public ThreadClient(Socket socket) throws IOException {
        this.socket = socket;
        this.cin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    @Override
    public void run() {
        try {
            while (true) {
                String message = cin.readLine();
                System.out.println(message);

                if(message.contains(MainActivity.SIG_REQ)==true) {
                    String [] mess = message.split("__");
                    String [] listeJouer = mess[1].split(",");
                    System.out.println("===================================================");
                    for(int i=0;i< listeJouer.length;i++){
                        System.out.println(listeJouer[i]);
                    }
                    System.out.println("===================================================");
                    MainActivity.listeJoueur = listeJouer;
                } else if(message.compareTo(MainActivity.LANCER_JEU)==0){
                    MainActivity.etatJeu = MainActivity.Etat_Jeu.LANCER;
                } else if(message.contains(MainActivity.CHANG_POS_PRE)==true) {
                    String [] poss = message.split("__");
                    String [] poss2 = poss[1].split(",");
                    MainActivity.positionX = Double.parseDouble(poss2[0]);
                    MainActivity.positionY = Double.parseDouble(poss2[1]);
                } else {
                    Communication communication = MainActivity.gson.fromJson(message, Communication.class);
                    MainActivity.game.getPlayer().updateAdversaire(communication);
                }
            }
        } catch (SocketException e) {
            System.out.println("You left the chat-room");
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            try {
                cin.close();
            } catch (Exception exception) {
                System.out.println(exception);
                exception.printStackTrace();
            }
        }
    }

}
