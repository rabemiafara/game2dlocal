package com.example.myapplication.objet;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.myapplication.Plan_secondaire;
import com.example.myapplication.utilisateur.Player;

public class Balle {

    private double position_x;
    private double position_y;

    private double debut_x;
    private double debut_y;

    private int vitesse ;
    private double velocity_X;
    private double velocity_Y;

    private Player player;
    private Paint paint;
    private Plan_secondaire plan_secondaire;
    private boolean valide;

    public Balle(Player players, Plan_secondaire planSecondaire,double velocitX,double velocitY){

        valide = false;

        this.plan_secondaire = planSecondaire;

        this.velocity_X = velocitX;
        this.velocity_Y = velocitY;

        this.player = players;
        this.position_x = this.player.getPositionX()+0;
        this.position_y = this.player.getPositionY()+0;

        this.debut_x = this.position_x + 0;
        this.debut_y = this.position_y + 0;

        this.paint = new Paint();
        this.paint.setColor(Color.YELLOW);
    }

    public boolean distanceMax(){
        double dist = Math.sqrt(
                Math.pow(this.position_x-this.debut_x,2)+
                        Math.pow(this.position_y-this.debut_y,2)
        );

        if(dist>=700){
            return true;
        }
        return false;
    }

    public void update(){
        this.position_x += this.velocity_X;
        this.position_y += this.velocity_Y;

        double dist = Math.sqrt(
                Math.pow(this.position_x-this.debut_x,2)+
                        Math.pow(this.position_y-this.debut_y,2)
        );

        if(dist>=150){
            this.valide = true;
        } else {
            this.valide = false;
        }

    }

    public void draw(Canvas canvas){
        canvas.drawCircle(
                (float)this.plan_secondaire.get_x_screen(this.position_x),
                (float)this.plan_secondaire.get_y_screen(this.position_y),
                10,
                paint
        );
    }
    public void draw(Canvas canvas,Plan_secondaire plan_secondaire1){
        canvas.drawCircle(
                (float)plan_secondaire1.get_x_screen(this.position_x),
                (float)plan_secondaire1.get_y_screen(this.position_y),
                10,
                paint
        );
    }

    public double getPosition_x() {
        return position_x;
    }

    public void setPosition_x(double position_x) {
        this.position_x = position_x;
    }

    public double getPosition_y() {
        return position_y;
    }

    public void setPosition_y(double position_y) {
        this.position_y = position_y;
    }

    public int getVitesse() {
        return vitesse;
    }

    public void setVitesse(int vitesse) {
        this.vitesse = vitesse;
    }

    public double getVelocity_X() {
        return velocity_X;
    }

    public void setVelocity_X(double velocity_X) {
        this.velocity_X = velocity_X;
    }

    public double getVelocity_Y() {
        return velocity_Y;
    }

    public void setVelocity_Y(double velocity_Y) {
        this.velocity_Y = velocity_Y;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean isValide() {
        return valide;
    }
}
