package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class Acceuille extends AppCompatActivity {

    private Button valider;
    private EditText champ_nom;

    public static String nom_joueur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics displayMetrics = new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        champ_nom = new EditText(this);

        valider = new Button(this);
        valider.setText("valider");
        valider.setBackgroundColor(Color.BLUE);

        LinearLayout linearLayout = new LinearLayout(this);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        linearLayout.setLayoutParams(layoutParams);

        champ_nom.setX(width/2);
        champ_nom.setY(100);

        valider.setX(width/2);
        valider.setY(300);

        linearLayout.addView(champ_nom);
        linearLayout.addView(valider);


        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.nom_joueur = champ_nom.getText().toString();
                System.out.println(MainActivity.nom_joueur);
                Intent intent = new Intent(Acceuille.this, Choix_jeu.class);
                startActivity(intent);
            }
        });

        setContentView(linearLayout);
    }
}


