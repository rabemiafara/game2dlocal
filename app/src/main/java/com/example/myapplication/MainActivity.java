package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.myapplication.net.client.Client;
import com.example.myapplication.net.server.NetworkThread;
import com.google.gson.Gson;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    public static String nom_joueur = "noob" ;
    public static String ipAddresse = "127.0.0.1" ;
    public static NetworkThread networkThread;
    public static Client client;
    public static final String SIG_REQ = "$__";
    public static final String REQ_LIST = "/liste";
    public static final String CHANG_POS = "POS__";
    public static final String CHANG_POS_PRE = "POS_PRE__";
    public static String [] listeJoueur;
    public static final String LANCER_JEU = "/lancer";
    public static Gson gson = new Gson();
    public static Game game;
    public static enum Etat_connection{
        HOST,
        CLIENT,
        NOT_CONNECTED
    }
    public static double positionX = 0;
    public static double positionY = 0;

    public static enum Etat_Jeu{
        LANCER,
        ATTENTE
    }
    public static Etat_connection etat_connection = Etat_connection.NOT_CONNECTED;
    public static Etat_Jeu etatJeu = Etat_Jeu.ATTENTE;
    public static String [] getListeJoueurServer(){

        try {
            HashMap<Socket, String> clientNameList = MainActivity.networkThread.getClientNameList();

            ArrayList <Socket> clients = MainActivity.networkThread.getClients();

            String [] liste_nom = new String[clients.size()];
            String [] ver_liste = new String[liste_nom.length+1];

            for(int i=0;i<liste_nom.length;i++){
                liste_nom[i] = clientNameList.get(clients.get(i)) ;
                ver_liste[i] = liste_nom[i]+"";
            }
            ver_liste[liste_nom.length] = MainActivity.nom_joueur ;
            listeJoueur = ver_liste;
            return  ver_liste ;
        } catch (Exception e){
            e.printStackTrace();
        }

        return null;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this,Acceuille.class);

        startActivity(intent);

//        setContentView(R.layout.activity_main);
    }

//    private void changeActivity(){
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                boolean lancer = false;
//                while (true){
//                    if(MainActivity.etatJeu==Etat_Jeu.LANCER){
//                        Intent intent = new
//
//                    }
//                }
//
//            }
//        }).start();
//    }

}
