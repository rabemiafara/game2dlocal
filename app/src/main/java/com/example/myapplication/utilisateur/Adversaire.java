package com.example.myapplication.utilisateur;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import androidx.annotation.NonNull;

import com.example.myapplication.MainActivity;
import com.example.myapplication.Plan_secondaire;
import com.example.myapplication.objet.Balle;
import com.google.gson.Gson;

import java.net.Socket;
import java.util.ArrayList;

public class Adversaire {

    private Paint paint;
    private double positionX;
    private double positionY;
    private Socket socket;
    private double fin_vise_x;
    private double fin_vise_y;
    private double distane_balle = 500;
    private ArrayList<Balle> balles ;
    private ArrayList<Integer> liste_remove;
    private String nom;
    private Communication communication;
    private Paint painte_vise;
    private double radius;
    private Player player;
    public Adversaire( String nom,Player pl ) {
        this.nom = nom;
        this.radius = 20;
        this.player = pl;
        this.paint = new Paint();
        paint.setColor(Color.RED);
        this.painte_vise = new Paint();
        painte_vise.setColor(Color.GREEN);
    }


    public void draw(Canvas canvas){

        canvas.drawCircle(
                (float) this.player.getPlan_secondaire().get_x_screen(positionX),
                (float) this.player.getPlan_secondaire().get_y_screen(positionY),
                (float) radius ,
                paint
        );

        canvas.drawLine(
                (float) this.player.getPlan_secondaire().get_x_screen(positionX) ,
                (float) this.player.getPlan_secondaire().get_y_screen(positionY) ,
                (float) this.player.getPlan_secondaire().get_x_screen(this.fin_vise_x) ,
                (float) this.player.getPlan_secondaire().get_y_screen(this.fin_vise_y) ,
                painte_vise
        );

//        for(int i=0;i<balles.size();i++){
//            balles.get(i).draw(canvas,this.player.getPlan_secondaire());
//        }

    }

    public void update(String json){
        communication = MainActivity.gson.fromJson(json, Communication.class);
        this.positionX = communication.getPositionX();
        this.positionY = communication.getPositionY();
        this.fin_vise_x = communication.getFin_tire_x();
        this.fin_vise_y = communication.getFin_tire_y();
    }

    public void update(@NonNull Communication communication1){
        this.positionX = communication1.getPositionX();
        this.positionY = communication1.getPositionY();
        this.fin_vise_x = communication1.getFin_tire_x();
        this.fin_vise_y = communication1.getFin_tire_y();
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}








