package com.example.myapplication.utilisateur;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.myapplication.Gameloop;
import com.example.myapplication.MainActivity;
import com.example.myapplication.Plan_secondaire;
import com.example.myapplication.bouton.Joystick_L;
import com.example.myapplication.bouton.Joystick_R;
import com.example.myapplication.objet.Balle;
import com.example.myapplication.obstacle.Obstacle;
import com.google.gson.Gson;

import java.util.ArrayList;
public class Player  {
    public static final double SPEED_PIXELS_PER_SECOND = 300.0;
    private static final double MAX_SPEED = SPEED_PIXELS_PER_SECOND / Gameloop.MAX_UPS;
    private Joystick_L joystickL;
    private Joystick_R joystickR;
    private double velocityX;
    private double velocityY;
    private double velocity_viseX =0 ;
    private double velocity_viseY =0;
    private double positionX ;
    private double positionY ;
    private double position_screen_X ;
    private double position_screen_Y ;
    private double radius;
    private Plan_secondaire plan_secondaire;
    private double screen_width;
    private double screen_height;
    private Paint paint;
    private Paint painte_vise;
    private Obstacle obstacle;
    private double fin_vise_x;
    private double fin_vise_y;
    private double distane_balle = 500;
    private ArrayList<Balle> balles ;
    private ArrayList<Integer> liste_remove;
    private String nom_joueur;
    private Communication communication;
    private ArrayList<Adversaire> adversaireArrayList;
    public Player(Joystick_L joystic,Joystick_R joystick_r ,Plan_secondaire plan_secondair, double ecran_width, double ecran_height, Obstacle obstacl){

        balles = new ArrayList<>();
        liste_remove = new ArrayList<>();
        this.radius = 20;
        this.plan_secondaire = plan_secondair;
        this.obstacle = obstacl;
        this.screen_width = ecran_width;
        this.screen_height = ecran_height;
        this.positionX = MainActivity.positionX + 0;
        this.positionY = MainActivity.positionY + 0;
        this.painte_vise = new Paint();
        this.painte_vise.setColor(Color.RED);
        this.fin_vise_x = this.positionX + 100;
        this.fin_vise_y = this.positionY + 100;
        this.position_screen_X = this.positionX + 0;
        this.position_screen_Y = this.positionY + 0;
        this.joystickL = joystic;
        this.joystickL.setPlayer(this);
        paint = new Paint();
        paint.setColor(Color.GREEN);
        this.joystickR = joystick_r;
        this.joystickR.setPlayer(this);
        this.communication = new Communication();
        this.communication.setNom_joueur(MainActivity.nom_joueur);
        getAdversaire();
    }

    public void getAdversaire(){
        String [] listeadv = MainActivity.listeJoueur;
        adversaireArrayList = new ArrayList<>();
        System.out.println("-------------------------------------------------------------");
        for(int i=0;i<listeadv.length;i++){
            if(listeadv[i].compareTo(MainActivity.nom_joueur)!=0){
                adversaireArrayList.add(new Adversaire(listeadv[i],this));
            }
        }
        System.out.println("-------------------------------------------------------------");
    }

    public void draw(Canvas canvas){

        canvas.drawCircle((float) position_screen_X ,(float) position_screen_Y ,(float) radius,paint);
        canvas.drawLine((float) position_screen_X ,
                (float) position_screen_Y,
                (float) this.plan_secondaire.get_x_screen(this.fin_vise_x) ,
                (float) this.plan_secondaire.get_y_screen(this.fin_vise_y) ,
                painte_vise);

        for(int i=0;i<balles.size();i++){
            balles.get(i).draw(canvas);
        }

        for(int i=0;i<adversaireArrayList.size() ;i++){
            adversaireArrayList.get(i).draw(canvas);
        }

    }

    public void tire() {
        System.out.println("le player tire il tire");

        this.velocity_viseX = joystickR.getActuatorX()*MAX_SPEED ;
        this.velocity_viseY = joystickR.getActuatorY()*MAX_SPEED ;


        int index = this.balles.size();

        if(index>0){
            Balle b = this.balles.get(index-1);
            if(b.isValide()==false){
                return;
            }
        }

        Balle balle = new Balle(
                this,
                this.plan_secondaire,
                this.velocity_viseX,
                this.velocity_viseY
        );

        balles.add(balle);

    }

    public void verifficationBalle(){

        liste_remove.clear();

        for(int i=0;i<balles.size();i++){
            if(balles.get(i).distanceMax()==true){
                balles.remove(i);
            }
        }

        for(int i=0;i<balles.size();i++){
            balles.get(i).update();
            if(obstacle.is_containse(balles.get(i).getPosition_x(),balles.get(i).getPosition_y())==false){
                liste_remove.add(i);
            }
        }

        for(int i=0;i<liste_remove.size();i++){
            int index = liste_remove.get(i);
            balles.remove(index);
        }

    }

    public void update_orientation(){

        verifficationBalle();

        for(int i=0;i<balles.size();i++){
            balles.get(i).update();
        }

        this.velocity_viseX = joystickR.getActuatorX()*MAX_SPEED ;
        this.velocity_viseY = joystickR.getActuatorY()*MAX_SPEED ;

        double x2 = this.positionX + 0;
        double y2 = this.positionY + 0;

        double distance = 0;

        if(this.velocity_viseY==0){
            this.velocity_viseY = 1;
        }

        if(this.velocity_viseX==0){
            this.velocity_viseX = 1;
        }


        while(true ){
            x2 += this.velocity_viseX;
            y2 += this.velocity_viseY;
            distance = Math.sqrt(
              Math.pow(x2 - this.positionX,2 ) +
              Math.pow(y2 - this.positionY,2 )
            );
            if(distance>=this.distane_balle){
                break;
            }
        }

        this.fin_vise_x = x2;
        this.fin_vise_y = y2;

        if(joystickR.isTire()==true){
            tire();
        }

    }

    public void update() {

        update_orientation();

        // Update velocity based on actuator of joystick
        velocityX = joystickL.getActuatorX()*MAX_SPEED;
        velocityY = joystickL.getActuatorY()*MAX_SPEED;
        // Update position

        if(this.obstacle.is_containse( positionX + velocityX,positionY + velocityY)==false){
            return;
        }

        positionX += velocityX;
        positionY += velocityY;

        if(this.plan_secondaire.get_x_screen(positionX) < 0 ) {
            this.plan_secondaire.setOrigine_x(position_screen_X - positionX);
        }

        if (this.plan_secondaire.get_x_screen(positionX) > this.screen_width ) {
            this.plan_secondaire.setOrigine_x(position_screen_X - positionX);
        }

        if( this.plan_secondaire.get_y_screen(positionY) < 0 ) {
            this.plan_secondaire.setOrigine_y(position_screen_Y - positionY);
        }
        if(this.plan_secondaire.get_y_screen(positionY) > this.screen_height ) {
            this.plan_secondaire.setOrigine_y(position_screen_Y - positionY);
        }

        position_screen_X = this.plan_secondaire.get_x_screen(positionX);
        position_screen_Y = this.plan_secondaire.get_y_screen(positionY);


        this.communication.setPositionX(positionX);
        this.communication.setPositionY(positionY);
        this.communication.setFin_tire_x(fin_vise_x);
        this.communication.setFin_tire_y(fin_vise_y);

        if(MainActivity.etat_connection == MainActivity.Etat_connection.HOST ){
            MainActivity.networkThread.sendMessage(MainActivity.gson.toJson(communication));
        } else {
            try {
                MainActivity.client.evoyerMessage(MainActivity.gson.toJson(communication)) ;
            } catch (Exception e){
                e.printStackTrace();
            }

        }

    }
    public double getPositionX() {
        return positionX;
    }
    public void setPositionX(double positionX) {
        this.positionX = positionX;
    }
    public double getPositionY() {
        return positionY;
    }
    public void setPositionY(double positionY) {
        this.positionY = positionY;
    }
    public Communication getCommunication() {
        return communication;
    }
    public void setCommunication(Communication communication) {
        this.communication = communication;
    }
    public Plan_secondaire getPlan_secondaire() {
        return plan_secondaire;
    }
    public void setPlan_secondaire(Plan_secondaire plan_secondaire) {
        this.plan_secondaire = plan_secondaire;
    }
    public void updateAdversaire(Communication communication){
        for(int i=0;i<adversaireArrayList.size();i++){
            if(adversaireArrayList.get(i).getNom().compareTo(communication.getNom_joueur())==0){
                adversaireArrayList.get(i).update(communication);
            }
        }
    }

}
