package com.example.myapplication.utilisateur;

import com.example.myapplication.objet.Balle;
import com.google.gson.Gson;
import java.util.ArrayList;
public class Communication {
    private double positionX;
    private double positionY;
    private double fin_tire_x;
    private double fin_tire_y;
    private String nom_joueur;
    public Communication(double positionX, double positionY, double fin_tire_x, double fin_tire_y, ArrayList<Balle> balles) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.fin_tire_x = fin_tire_x;
        this.fin_tire_y = fin_tire_y;
    }
    public Communication() {

    }
    public String getJson(){
        return new Gson().toJson(this);
    }
    public double getPositionX() {
        return positionX;
    }
    public void setPositionX(double positionX) {
        this.positionX = positionX;
    }

    public double getPositionY() {
        return positionY;
    }

    public void setPositionY(double positionY) {
        this.positionY = positionY;
    }
    public double getFin_tire_x() {
        return fin_tire_x;
    }

    public void setFin_tire_x(double fin_tire_x) {
        this.fin_tire_x = fin_tire_x;
    }

    public double getFin_tire_y() {
        return fin_tire_y;
    }
    public void setFin_tire_y(double fin_tire_y) {
        this.fin_tire_y = fin_tire_y;
    }
    public String getNom_joueur() {
        return nom_joueur;
    }
    public void setNom_joueur(String nom_joueur) {
        this.nom_joueur = nom_joueur;
    }

}