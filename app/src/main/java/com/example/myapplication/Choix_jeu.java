package com.example.myapplication;

import static com.example.myapplication.MainActivity.etat_connection;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.net.client.Client;
import com.example.myapplication.net.server.NetworkThread;

public class Choix_jeu extends AppCompatActivity {



    private Button create_game;
    private Button join_game;
    private Button teste_connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView textView = new TextView(this);

        textView.setText("WELCOME "+MainActivity.nom_joueur);

        textView.setTextSize(50);

//        textView.setLayoutParams(new LinearLayout.LayoutParams(50,50));

        textView.setX(100);

        create_game = new Button(this);
        create_game.setText("cree un jeu");

        create_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if(etat_connection == MainActivity.Etat_connection.CLIENT ){
                        System.out.println("vous etes deja un CLIENT");
                        return;
                    }
                    if(etat_connection == MainActivity.Etat_connection.HOST){
                        System.out.println("vous etes deja un HOST");
                        return;
                    }
                    System.out.println("create a game");
                    MainActivity.networkThread = new NetworkThread();
                    MainActivity.networkThread.execute();

                    etat_connection = MainActivity.Etat_connection.HOST;

//                client = new Client(context);
//                client.start();

                    System.out.println("l'etat de son connection est HOST");

//                    Intent intent = new Intent(Choix_jeu.this,Liste_Client.class);
//                    startActivity(intent);

                } catch (Exception e){
                    e.printStackTrace();
                }


            }
        });

        join_game = new Button(this);
        join_game.setText("joindre un jeu");

        join_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etat_connection == MainActivity.Etat_connection.HOST){
                    System.out.println("vous etes deja un HOST");
                    return;
                }
                if(etat_connection == MainActivity.Etat_connection.CLIENT ){
                    System.out.println("vous etes deja un CLIENT");
                    return;
                }
                System.out.println("join a game");

                Intent intent = new Intent(Choix_jeu.this, Entry_IPServer.class);

                startActivity(intent);

                System.out.println("l'etat de son connection est CLIENT");

            }
        });

        teste_connection = new Button(this);
        teste_connection.setText("suivant");

        teste_connection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(etat_connection== MainActivity.Etat_connection.NOT_CONNECTED){
                    System.out.println("vous n'etes pas connecter");
                }
                else if(etat_connection== MainActivity.Etat_connection.HOST){
                    create_game.setText("vous etes deja un serveur non");

                    Intent intent = new Intent(Choix_jeu.this, Liste_Client.class);
                    startActivity(intent);

                }
                else if(etat_connection== MainActivity.Etat_connection.CLIENT){
                    join_game.setText("vous etes deja un client non");
                    System.out.println("encours d'envoye du message");
                    MainActivity.client.evoyerMessage("voicie une message du serveur");
                }
            }
        });


        LinearLayout linearLayout = new LinearLayout(this);

        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        ));
        linearLayout.setOrientation(LinearLayout.VERTICAL);



//        joystick.setLayoutParams(new LinearLayout.LayoutParams(100,100));

        linearLayout.addView(textView);
        linearLayout.addView(create_game);
        linearLayout.addView(join_game);
        linearLayout.addView(teste_connection);


        setContentView(linearLayout);
    }
}