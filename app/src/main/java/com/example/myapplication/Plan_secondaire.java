package com.example.myapplication;

import com.example.myapplication.bouton.Joystick_R;
import com.example.myapplication.utilisateur.Player;

public class Plan_secondaire {


    public static final double SPEED_PIXELS_PER_SECOND = 600.0;
    private static final double MAX_SPEED = SPEED_PIXELS_PER_SECOND / Gameloop.MAX_UPS;

    private double origine_x ;
    private double origine_y ;

    private Joystick_R joystick_r;
    private double velocityX;
    private double velocityY;
    private Player player;

    public Plan_secondaire(double x,double y,Joystick_R joy){
        this.joystick_r = joy;
        this.origine_x = x;
        this.origine_y = y;
    }

    public void update() {
        // Update velocity based on actuator of joystick
        velocityX = joystick_r.getActuatorX() * MAX_SPEED;
        velocityY = joystick_r.getActuatorY() * MAX_SPEED;
        // Update position

//        if(get_x_screen(player.getPositionX())<100 || get_x_screen(player.getPositionX())>=900 ){
//
//        } else {
//            origine_x -= velocityX;
//        }
//
//        if(get_y_screen(player.getPositionX())<100 || get_y_screen(player.getPositionY())>=600 ){
//
//        } else {
//            origine_y -= velocityY;
//        }




//        this.plan_secondaire.setOrigine_x(position_screen_X - positionX);
//        this.plan_secondaire.setOrigine_y(position_screen_Y - positionY);

//        position_screen_X = this.plan_secondaire.get_x_screen(positionX);
//        position_screen_Y = this.plan_secondaire.get_y_screen(positionY);

//        if(this.plan_secondaire.get_x_screen(positionX) < 0 ) {
//            this.plan_secondaire.setOrigine_x(position_screen_X - positionX);
//        }
//
//        if (this.plan_secondaire.get_x_screen(positionX) > this.screen_width ) {
//            this.plan_secondaire.setOrigine_x(position_screen_X - positionX);
//        }
//
//        if( this.plan_secondaire.get_y_screen(positionY) < 0 ) {
//            this.plan_secondaire.setOrigine_y(position_screen_Y - positionY);
//        }
//        if(this.plan_secondaire.get_y_screen(positionY) > this.screen_height ) {
//            this.plan_secondaire.setOrigine_y(position_screen_Y - positionY);
//        }
//
//        position_screen_X = this.plan_secondaire.get_x_screen(positionX);
//        position_screen_Y = this.plan_secondaire.get_y_screen(positionY);

//
//        position_screen_X = this.plan_secondaire.get_x_screen(positionX);
//        position_screen_Y = this.plan_secondaire.get_y_screen(positionY);

//        System.out.println("==================================================================================================");
//        System.out.println("==================================================================================================");
//
//        System.out.println("origine =>");
//        this.plan_secondaire.print_origines();
//        System.out.println(" ");
//        System.out.println("position actuele => ");
//        System.out.println("position du personnage "+positionX+" <---> "+positionY);
//        System.out.println(" ");
//        System.out.println("position dans l ' ecran => ");
//        System.out.println(position_screen_X+";;;;;;"+position_screen_Y);
//
//        System.out.println("==================================================================================================");
//        System.out.println("==================================================================================================");

//        this.plan_secondaire.print_origines();



    }


    public void print_origines(){
        System.out.println(this.origine_x+"<====>"+this.origine_y);
    }

    public double get_x_screen(double x){
        return this.origine_x + x;
    }

    public double get_y_screen(double y){
        return this.origine_y + y;
    }

    public double getOrigine_x() {
        return origine_x;
    }

    public void setOrigine_x(double origine) {
        System.out.println("premier origine => "+this.origine_x+" se transforme en "+origine);
        this.origine_x = origine;
    }

    public double getOrigine_y() {
        return origine_y;
    }

    public void setOrigine_y(double origine) {
        this.origine_y = origine;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
