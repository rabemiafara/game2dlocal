package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.myapplication.net.client.Client;

public class Entry_IPServer extends AppCompatActivity {
    private Button valider;

    private Button suivant;
    private EditText champ_IP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics displayMetrics = new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        champ_IP = new EditText(this);

        suivant = new Button(this);
        suivant.setText("suivant");
        suivant.setClickable(false);

        valider = new Button(this);
        valider.setText("valider");
        valider.setBackgroundColor(Color.BLUE);

        LinearLayout linearLayout = new LinearLayout(this);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        linearLayout.setLayoutParams(layoutParams);

        champ_IP.setX(width/2);
        champ_IP.setY(100);

        valider.setX(width/2);
        valider.setY(300);

        suivant.setX(width/2);
        suivant.setY(360);

        linearLayout.addView(champ_IP);
        linearLayout.addView(valider);
        linearLayout.addView(suivant);


        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.ipAddresse = champ_IP.getText().toString();
                System.out.println(MainActivity.ipAddresse);
                MainActivity.client = new Client();
                MainActivity.client.execute();
                MainActivity.etat_connection = MainActivity.Etat_connection.CLIENT;
                System.out.println("vous etes connecter");
                suivant.setClickable(true);

            }
        });
        suivant.setBackgroundColor(Color.BLUE);

        suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.client.evoyerMessage(MainActivity.REQ_LIST);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        boolean misive = false;
                        while (true){
                            try {
                                if(MainActivity.listeJoueur.length!=0){
                                    misive = true;
                                    break;
                                }
                            } catch (Exception e){

                            }
                        }
                        if(misive==true){
                            Intent intent = new Intent(Entry_IPServer.this,Liste_Client.class);
                            startActivity(intent);
                        }
                    }
                }).start();


            }
        });

        setContentView(linearLayout);

    }
}