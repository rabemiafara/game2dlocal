package com.example.myapplication.page;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.myapplication.Acceuille;
import com.example.myapplication.MainActivity;
import com.example.myapplication.net.client.Client;
import com.example.myapplication.net.server.NetworkThread;

public class Acceuille_page  {

    private enum Etat_connection{
        HOST,
        CLIENT,
        NOT_CONNECTED
    }

    private Button create_game;
    private Button join_game;
    private Button teste_connection;

    private Etat_connection etat_connection ;

    NetworkThread networkThread;
    Client client;

    public LinearLayout ecrand_acceuille(final Context context){

        etat_connection = Etat_connection.NOT_CONNECTED;

        TextView textView = new TextView(context);

        textView.setText("WELCOME !!!"+ MainActivity.nom_joueur);

        textView.setTextSize(50);

//        textView.setLayoutParams(new LinearLayout.LayoutParams(50,50));

        textView.setX(100);

        create_game = new Button(context);
        create_game.setText("cree un jeu");

        create_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etat_connection ==Etat_connection.CLIENT ){
                    System.out.println("vous etes deja un CLIENT");
                    return;
                }
                if(etat_connection == Etat_connection.HOST){
                    System.out.println("vous etes deja un HOST");
                    return;
                }
                System.out.println("create a game");
                networkThread = new NetworkThread();
                networkThread.execute();
                etat_connection = Etat_connection.HOST;

//                client = new Client(context);
//                client.start();

                System.out.println("l'etat de son connection est HOST");
            }
        });

        join_game = new Button(context);
        join_game.setText("joindre un jeu");

        join_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etat_connection == Etat_connection.HOST){
                    System.out.println("vous etes deja un HOST");
                    return;
                }
                if(etat_connection ==Etat_connection.CLIENT ){
                    System.out.println("vous etes deja un CLIENT");
                    return;
                }
                System.out.println("join a game");
//                Toast.makeText(context,"join a game",Toast.LENGTH_LONG).show();
                client = new Client();
                client.execute() ;
                etat_connection=Etat_connection.CLIENT;
                System.out.println("l'etat de son connection est CLIENT");
            }
        });

        teste_connection = new Button(context);
        teste_connection.setText("faire le teste");

        teste_connection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(etat_connection==Etat_connection.NOT_CONNECTED){
                    System.out.println("vous n'etes pas connecter");
                }
                else if(etat_connection==Etat_connection.HOST){
                    create_game.setText("vous etes deja un serveur non");
                }
                else if(etat_connection==Etat_connection.CLIENT){
                    join_game.setText("vous etes deja un client non");
                    System.out.println("encours d'envoye du message");
                    client.evoyerMessage("voicie une message du serveur");
                }
            }
        });


        LinearLayout linearLayout = new LinearLayout(context);

        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        ));
        linearLayout.setOrientation(LinearLayout.VERTICAL);



//        joystick.setLayoutParams(new LinearLayout.LayoutParams(100,100));

        linearLayout.addView(textView);
        linearLayout.addView(create_game);
        linearLayout.addView(join_game);
        linearLayout.addView(teste_connection);

        return linearLayout ;

    }


}
