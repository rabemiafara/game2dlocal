package com.example.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.myapplication.bouton.Joystick_L;
//import com.example.myapplication.personnage.Avatar;

import com.example.myapplication.bouton.Joystick_R;
import com.example.myapplication.obstacle.Obstacle;
import com.example.myapplication.utilisateur.Adversaire;
import com.example.myapplication.utilisateur.Player;

import java.util.ArrayList;


public class Game extends SurfaceView implements SurfaceHolder.Callback {

    private Gameloop gameLoop;

    private Joystick_L joystickL;
    private Joystick_R joystickR;

    private Player player;
    private int numberOfSpellsToCast = 0;
    private int joystickPointerId = 0;
    private int joystickPointerId_R = 0;
    private Gameloop gameloop;
    private Plan_secondaire plan_secondaire;
    private DisplayMetrics displayMetrics ;
    private double screen_width;
    private double screen_height;

    private Obstacle obstacle;

    private Screem_JoyStick_2 screem_joyStick_2;
    private ArrayList <Adversaire> listeAdversaire ;
    public Game(Context context,double ecran_width,double ecran_height,Screem_JoyStick_2 screem) {
        super(context);

//        listeAdversaires = new Adversaire[MainActivity.listeJoueur.length-1] ;

        listeAdversaire = new ArrayList<>();

        for(int i=0;i<MainActivity.listeJoueur.length;i++){
            if(MainActivity.listeJoueur[i].compareTo(MainActivity.nom_joueur)!=0){
                Adversaire adversaire = new Adversaire(MainActivity.listeJoueur[i],this.player);
                listeAdversaire.add(adversaire);
            }
        }

        this.screen_width = ecran_width;
        this.screen_height = ecran_height;

        this.screem_joyStick_2 = screem;

        SurfaceHolder surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);

        this.plan_secondaire = new Plan_secondaire(0,0,this.screem_joyStick_2.getJoystickR());

        gameLoop = new Gameloop(this, surfaceHolder);

        int joy_y = (int)((this.screen_height/3)*2);
        int joy_x = (int)((this.screen_width/3)) - 60;

        joystickL = new Joystick_L(joy_x, joy_y, 70, 40);

        int r_joy_x = (int)((this.screen_width/3)*2) - 60;
        int r_joy_y = (int)((this.screen_height/3)*2);

        joystickR = new Joystick_R(r_joy_x,r_joy_y,70,40);

        obstacle = new Obstacle(400,50,this.plan_secondaire);

        this.player = new Player(joystickL, this.screem_joyStick_2.getJoystickR() ,this.plan_secondaire,ecran_width,ecran_height,obstacle);


        this.plan_secondaire.setPlayer(this.player);

        setFocusable(true);

    }

    public void drawUPS(Canvas canvas){
        String averagUAPS = Double.toString(gameLoop.getAverageUPS());
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setTextSize(30);
        canvas.drawText("UPS =>"+averagUAPS,200,40,paint );
    }

    public void drawFPS(Canvas canvas){
        String averageFPS = Double.toString(gameLoop.getAverageFPS());
        Paint paint = new Paint();
        paint.setColor(Color.GREEN);
        paint.setTextSize(30);
        canvas.drawText("FPS => "+averageFPS,200,80,paint);

    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        // Draw Tilemap
        drawFPS(canvas);
        drawUPS(canvas);
        joystickL.draw(canvas);
        player.draw(canvas);
        obstacle.draw(canvas);
//        avatar.draw(canvas);

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d("Game.java", "surfaceChanged()");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d("Game.java", "surfaceDestroyed()");
    }



    public void update() {
        // Stop updating the game if the player is dead
        plan_secondaire.update();
        joystickL.update();
        player.update();
        obstacle.update();

    }

    public void pause() {
        gameLoop.stopLoop();
    }

//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//
//        // Handle user input touch event actions
//        switch (event.getActionMasked()) {
//            case MotionEvent.ACTION_DOWN:
//            case MotionEvent.ACTION_POINTER_DOWN:
//                if (joystickL.getIsPressed()) {
//                    // Joystick was pressed before this event -> cast spell
//                    numberOfSpellsToCast ++;
//                    System.out.println("boutton clicker");
//                } else if (joystickL.isPressed((double) event.getX(), (double) event.getY())) {
//                    // Joystick is pressed in this event -> setIsPressed(true) and store pointer id
//                    joystickPointerId = event.getPointerId(event.getActionIndex());
//                    System.out.println("boutton clicker");
//                    joystickL.setIsPressed(true);
//                } else {
//                    // Joystick was not previously, and is not pressed in this event -> cast spell
//                    numberOfSpellsToCast ++;
//                }
//
//
//                if (joystickR.getIsPressed()) {
//                    // Joystick was pressed before this event -> cast spell
//                    numberOfSpellsToCast ++;
//                    System.out.println("boutton clicker");
//                } else if (joystickR.isPressed((double) event.getX(), (double) event.getY())) {
//                    // Joystick is pressed in this event -> setIsPressed(true) and store pointer id
//                    joystickPointerId_R = event.getPointerId(event.getActionIndex());
//                    System.out.println("boutton clicker");
//                    joystickR.setIsPressed(true);
//                } else {
//                    // Joystick was not previously, and is not pressed in this event -> cast spell
//                    numberOfSpellsToCast ++;
//                }
//
//
//                return true;
//            case MotionEvent.ACTION_MOVE:
//
////                int pointer_counte = event.getPointerCount();
//
////                for (int i=0;i<pointer_counte;i++){
////                    int id = event.getPointerId(i);
////                    if(id == joystickPointerId){
////                        if (joystickL.getIsPressed()) {
////                            // Joystick was pressed previously and is now moved
////                            joystickL.setActuator((double) event.getY(id), (double) event.getY(id));
////                        }
////                    }
////                    if(id == joystickPointerId_R){
////                        if (joystickR.getIsPressed()) {
////                            // Joystick was pressed previously and is now moved
////                            joystickR.setActuator((double) event.getX(id), (double) event.getY(id));
////                        }
////                    }
////                }
//                if (joystickL.getIsPressed()) {
//                    // Joystick was pressed previously and is now moved
//                    joystickL.setActuator((double) event.getY(), (double) event.getY());
//                }
//
//                if (joystickR.getIsPressed()) {
//                    // Joystick was pressed previously and is now moved
//                    joystickR.setActuator((double) event.getX(joystickPointerId_R), (double) event.getY(joystickPointerId_R));
//                }
//
//                return true;
//
//            case MotionEvent.ACTION_UP:
//            case MotionEvent.ACTION_POINTER_UP:
//                if (joystickPointerId == event.getPointerId(event.getActionIndex())) {
//                    // joystick pointer was let go off -> setIsPressed(false) and resetActuator()
//                    joystickL.setIsPressed(false);
//                    joystickL.resetActuator();
//                }
//
//                if (joystickPointerId_R == event.getPointerId(event.getActionIndex())) {
//                    // joystick pointer was let go off -> setIsPressed(false) and resetActuator()
//                    joystickR.setIsPressed(false);
//                    joystickR.resetActuator();
//                }
//
//                return true;
//        }
//
//        return super.onTouchEvent(event);
//    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // Handle user input touch event actions
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                if (joystickL.getIsPressed()) {
                    // Joystick was pressed before this event -> cast spell
                    numberOfSpellsToCast ++;
                    System.out.println("boutton clicker");
                } else if (joystickL.isPressed((double) event.getX(), (double) event.getY())) {
                    // Joystick is pressed in this event -> setIsPressed(true) and store pointer id
                    joystickPointerId = event.getPointerId(event.getActionIndex());
                    System.out.println("boutton clicker");
                    joystickL.setIsPressed(true);
                } else {
                    // Joystick was not previously, and is not pressed in this event -> cast spell
                    numberOfSpellsToCast ++;
                }
                return true;
            case MotionEvent.ACTION_MOVE:
                if (joystickL.getIsPressed()) {
                    // Joystick was pressed previously and is now moved
                    joystickL.setActuator((double) event.getX(), (double) event.getY());
                }
                return true;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                if (joystickPointerId == event.getPointerId(event.getActionIndex())) {
                    // joystick pointer was let go off -> setIsPressed(false) and resetActuator()
                    joystickL.setIsPressed(false);
                    joystickL.resetActuator();
                }
                return true;



        }

        return super.onTouchEvent(event);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d("Game.java", "surfaceCreated()");
        if (gameLoop.getState().equals(Thread.State.TERMINATED)) {
            SurfaceHolder surfaceHolder = getHolder();
            surfaceHolder.addCallback(this);
            gameLoop = new Gameloop(this, surfaceHolder);
        }
        gameLoop.startLoop();
    }

    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }
}